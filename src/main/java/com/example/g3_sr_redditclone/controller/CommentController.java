package com.example.g3_sr_redditclone.controller;


import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Post;
import com.example.g3_sr_redditclone.service.CommentService;
import com.example.g3_sr_redditclone.service.PostService;
import com.example.g3_sr_redditclone.service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CommentController {

    @Autowired
    SubredditService subredditService;

    @Autowired
    PostService postService;

    @Autowired
    CommentService commentService;

    @GetMapping("/handle-upvoteComment/{id}")
    public String upvoteComment(@PathVariable int id, Model model, @ModelAttribute Comment comment){

        Comment comment1 = commentService.findCommentById(id);
        Comment newComment = new Comment();

        int count = 0;
        if (comment1.getVote_count()== 0){
            count = 1;
        }
        if (comment1.getVote_count() == 1){
            count = 0;
        }
        if (comment1.getVote_count() == -1){
            count = 1;
        }
        newComment.setVote_count(count);
        commentService.updateVote(newComment, comment1.getComment_id());

        return "redirect:/add-comment/" +comment1.getPost_id();
    }

    @GetMapping("/handle-downvoteComment/{id}")
    public String downvoteComment(@PathVariable int id, Model model,@ModelAttribute Comment comment){

        Comment comment1 = commentService.findCommentById(id);
        Comment newComment = new Comment();

        int count = 0;
        if (comment1.getVote_count() == 0){
            count = -1;
        }
        if (comment1.getVote_count() == 1){
            count = -1;
        }
        if (comment1.getVote_count() == -1){
            count = 0;
        }
        newComment.setVote_count(count);
        commentService.updateVote(newComment, comment1.getComment_id());

        return "redirect:/add-comment/" +comment1.getPost_id();
    }



    @GetMapping("/add-comment/{id}")
    public String addComment(@PathVariable int id, Model model){
        Post result = postService.findPostById(id);

        model.addAttribute("post", result);
        model.addAttribute("subreddits", subredditService.getSubTitle(id));
        model.addAttribute("comment", new Comment());
        model.addAttribute("posts", postService.findPostById(id));
        model.addAttribute("mainId", id);
        model.addAttribute("comments", commentService.findCommentByPostId(id));

        return "readPost";
    }

    @PostMapping("/handle-addComment/{id}")
    public String handleAddComment(@ModelAttribute Comment comment, BindingResult bindingResult, @PathVariable int id, Model model){
        System.out.println("testing comment");

        if (bindingResult.hasErrors()){
            return "add-comment";
        }
        System.out.println(comment);
        comment.setPost_id(id);

        commentService.insertComment(comment);
        model.addAttribute(comment);
        return "redirect:/add-comment/" + id;
    }

}
