package com.example.g3_sr_redditclone.controller;


import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Post;
import com.example.g3_sr_redditclone.model.Subreddit;
import com.example.g3_sr_redditclone.service.CommentService;
import com.example.g3_sr_redditclone.service.FileStorageService;
import com.example.g3_sr_redditclone.service.PostService;
import com.example.g3_sr_redditclone.service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class PostController {

    @Autowired
    SubredditService subredditService;

    @Autowired
    PostService postService;

    @Autowired
    CommentService commentService;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping("/redditClone")
    public String homePage(Model model){
        System.out.println("hi this is the main page");
        postService.getAllPost().stream().forEach(System.out::println);

        List<Post> allPost  = postService.getAllPost();
        model.addAttribute("allPost", allPost);
        return "homePage";
    }
    @GetMapping("/handle-upvoteSameSub/{id}")
    public String upvoteSameSub(@PathVariable int id){
        Post post = postService.findPostById(id);
        Post newPost = new Post();
        int count = 0;
        if (post.getVote_count() == 0){
            count = 1;
        }
        if (post.getVote_count() == 1){
            count = 0;
        }
        if (post.getVote_count() == -1){
            count = 1;
        }
        System.out.println(count);
        newPost.setVote_count(count);
        System.out.println("Here is the value of the new post : "+newPost.toString());
        postService.updateVote(newPost, post.getPost_id());

        return "redirect:/getSelectedPost/" +post.getSubreddit().getSubreddit_id();
    }

    @GetMapping("/handle-downvoteSameSub/{id}")
    public String downvoteSameSub(@PathVariable int id){

        Post post = postService.findPostById(id);
        Post newPost = new Post();
        int count = 0;
        if (post.getVote_count() == 0){
            count = -1;
        }
        if (post.getVote_count() == 1){
            count = -1;
        }
        if (post.getVote_count() == -1){
            count = 0;
        }
        System.out.println(count);
        newPost.setVote_count(count);
        System.out.println("Here is the value of the new post : "+newPost.toString());
        postService.updateVote(newPost, post.getPost_id());

        return "redirect:/getSelectedPost/" +post.getSubreddit().getSubreddit_id();
    }
    @GetMapping("/handle-upvote/{id}")
    public String upvote(@PathVariable int id, Model model){
        System.out.println(id);
        Post post = postService.findPostById(id);
        Post newPost = new Post();
        int count = 0;
        if (post.getVote_count() == 0){
            count = 1;
        }
        if (post.getVote_count() == 1){
            count = 0;
        }
        if (post.getVote_count() == -1){
            count = 1;
        }
        System.out.println(count);
        newPost.setVote_count(count);
        System.out.println("Here is the value of the new post : "+newPost.toString());
        postService.updateVote(newPost, post.getPost_id());
        return "redirect:/home";
    }

    @GetMapping("/handle-downvote/{id}")
    public String downvote(@PathVariable int id, Model model){
        Post post = postService.findPostById(id);
        Post newPost = new Post();
        int count = 0;
        if (post.getVote_count() == 0){
            count = -1;
        }
        if (post.getVote_count() == -1){
            count = 0;
        }
        if (post.getVote_count() == 1){
            count = -1;
        }
        System.out.println(count);
        newPost.setVote_count(count);
        System.out.println("Here is the value of the new post : "+newPost.toString());
        postService.updateVote(newPost, post.getPost_id());
        return "redirect:/home";
    }

    @GetMapping("/handle-upvotePost/{id}")
    public String upvotePost(@PathVariable int id, Model model,@ModelAttribute Comment comment){
        Post post = postService.findPostById(id);
        Post newPost = new Post();
        int count = 0;
        if (post.getVote_count() == 0){
            count = 1;
        }
        if (post.getVote_count() == 1){
            count = 0;
        }
        if (post.getVote_count() == -1){
            count = 1;
        }
        System.out.println(count);
        newPost.setVote_count(count);
        System.out.println("Here is the value of the new post : "+newPost.toString());
        postService.updateVote(newPost, post.getPost_id());

        return "redirect:/add-comment/" +id;
    }

    @GetMapping("/handle-downvotePost/{id}")
    public String downvotePost(@PathVariable int id, Model model,@ModelAttribute Comment comment){

        Post post = postService.findPostById(id);
        Post newPost = new Post();
        int count = 0;
        if (post.getVote_count() == 0){
            count = -1;
        }
        if (post.getVote_count() == 1){
            count = -1;
        }
        if (post.getVote_count() == -1){
            count = 0;
        }
        System.out.println(count);
        newPost.setVote_count(count);
        System.out.println("Here is the value of the new post : "+newPost.toString());
        postService.updateVote(newPost, post.getPost_id());

        return "redirect:/add-comment/" +id;
    }


    @GetMapping("/editPost/{id}")
    public String showUpdatePost(@PathVariable int id, Model model){
        Post i = postService.findPostById(id);
        model.addAttribute("post",i);
        model.addAttribute("posts", new Post());
        model.addAttribute("subreddits",subredditService.getAllSubreddit());
        return "editPost";
    }

    @PostMapping("/handle-updatePost/{id}")
    public String editPost(@PathVariable int id, @ModelAttribute @Valid Post post, BindingResult bindingResult, Model model){
        System.out.println(post);
        if (bindingResult.hasErrors()){
            model.addAttribute("subreddits",subredditService.getAllSubreddit());
            return "/editPost";
        }
        if(!post.getFile().isEmpty()){
            try{
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(post.getFile());
                post.setPost_url(filename);
            }catch (IOException ex){
                System.out.println("Error with the image upload "+ex.getMessage());
            }
        }
        postService.updatePost(post, id);
        return "redirect:/home";
    }

    @GetMapping("/getSelectedPost/{id}")
    public String selectedPost(@PathVariable int id, Model model){
        Subreddit sub = subredditService.findSubredditById(id);
        List<Post> post = postService.findPostBySubredditId(sub.getSubreddit_id());

        model.addAttribute("subreddits",subredditService.getAllSubreddit());
        model.addAttribute("posts",post);
        List<Comment> com = commentService.getAllComment();
        model.addAttribute("comment", com);
        return "selectedPost";
    }

    @GetMapping("/post_reddit")
    public String postReddit(Model model){
        model.addAttribute("post",new Post());
        model.addAttribute("subreddits",subredditService.getAllSubreddit());
        return "createPost";
    }

    @GetMapping(value = "/delete-post/{id}")
    public String deletePost(@PathVariable int id){
        postService.deletePost(id);
        return "redirect:/home";
    }

}
