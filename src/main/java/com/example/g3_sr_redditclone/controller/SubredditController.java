package com.example.g3_sr_redditclone.controller;


import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Post;
import com.example.g3_sr_redditclone.model.Subreddit;
import com.example.g3_sr_redditclone.service.CommentService;
import com.example.g3_sr_redditclone.service.FileStorageService;
import com.example.g3_sr_redditclone.service.PostService;
import com.example.g3_sr_redditclone.service.SubredditService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;

@Controller
public class SubredditController {

    @Autowired
    SubredditService subredditService;

    @Autowired
    PostService postService;

    @Autowired
    CommentService commentService;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping("/home")
    public String getAllSubreddit(Model model){
        List<Subreddit> allSub = subredditService.getAllSubreddit();

        model.addAttribute("subreddit", new Subreddit());
        model.addAttribute("subreddits", allSub);
        System.out.println(postService.getAllPost());

        List<Post> allPost = postService.getAllPost();
        model.addAttribute("posts", allPost);

        List<Post> post = postService.findCommentByPostId(allPost.size());

        model.addAttribute("post", postService.getAllPost());
        return "index";
    }
    @GetMapping("/create_subreddit")
    public String createSubReddit(Model model){
        model.addAttribute("subreddit", new Subreddit());
        return "createSubreddit";
    }

    @PostMapping("/handle-addSubreddit")
    public String handleAddSubreddit(@ModelAttribute @Valid Subreddit subreddit, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "/createSubreddit";
        }
        subredditService.insertSubreddit(subreddit);
        return "redirect:/home";
    }

    @GetMapping("/view_all")
    public String viewAll(Model model){
        model.addAttribute("subreddit", new Subreddit());
        model.addAttribute("subreddits",subredditService.getAllSubreddit());
        return "viewAll";
    }




// not working
    @PostMapping("/handle-addPost")
    public String handleAdd(@ModelAttribute @Valid Post post, BindingResult bindingResult,Model model){
        if (post.getFile().isEmpty()){
            // set default profileImage ;
            System.out.println("this is empty");
        }
        else {
            System.out.println(post.getFile());
            try{
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(post.getFile());
                post.setPost_url(filename);
            }catch (IOException ex){
                System.out.println("Error with the image upload "+ex.getMessage());
            }
        }
        if (bindingResult.hasErrors()){
            model.addAttribute("subreddits",subredditService.getAllSubreddit());
            return "createPost";
        }
        try{
            postService.inserPost(post);
        }catch (Exception e){
            return "redirect:/home";
        }

        return  "redirect:/home";
    }

    @GetMapping(value = "/delete/{id}")
    public String deleteSubreddit(@PathVariable int id){
        subredditService.deleteSubreddit(id);
        return "redirect:/view_all";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable int id, Model model){
        Subreddit i = subredditService.findSubredditById(id);
        model.addAttribute("subreddit",i);
        return "editSubreddit";
    }

    @PostMapping("/handle-update/{id}")
    public String edit(@PathVariable int id,@ModelAttribute @Valid Subreddit subreddit, BindingResult bindingResult){
        Subreddit result = subredditService.findSubredditById(id);
        System.out.println(subreddit);
        System.out.println(result);
        if (bindingResult.hasErrors()){
            return "/editSubreddit";
        }
        System.out.println(subredditService.updateSubreddit(subreddit, result.getSubreddit_id()));
        return "redirect:/view_all";
    }




}
