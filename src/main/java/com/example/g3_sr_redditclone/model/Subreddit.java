package com.example.g3_sr_redditclone.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@Data
public class Subreddit {
    private int subreddit_id;
    @NotEmpty(message = "Please provide a title")
    private String subreddit_title;
    private String subreddit_last_update;
    private String subreddit_create_date;

    public Subreddit(String subreddit_title) {
        this.subreddit_title = subreddit_title;
    }
}
