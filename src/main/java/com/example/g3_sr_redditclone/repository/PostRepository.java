package com.example.g3_sr_redditclone.repository;

import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Post;
import com.example.g3_sr_redditclone.model.Subreddit;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {

    @Select("select * from post")
    @Results({
            @Result(property = "post_id", column = "post_id"),
            @Result(property = "post_title", column = "post_title"),
            @Result(property = "post_description", column = "post_description"),
            @Result(property = "post_url", column = "post_url"),
            @Result(property = "post_last_update", column = "last_update"),
            @Result(property = "vote_count", column = "vote_count"),
            @Result(property = "subreddit", column = "subreddit_id", many = @Many(select = "findSubredditById")),
            @Result(property = "comment", column = "post_id", many = @Many(select = "findCommentByPostId"))
    })
    List<Post> getAllPost();

    @Select("select * from subreddit where subreddit_id=#{id}")
    public Subreddit findSubredditById(int id);
    @Select("select * from comment where post_id=#{id}")
    List<Comment> findCommentBySubredditId(int id);


    @Select("select * from post where post_id = #{id}")
    @Results({
            @Result(property = "post_id", column = "post_id"),
            @Result(property = "post_title", column = "post_title"),
            @Result(property = "post_description", column = "post_description"),
            @Result(property = "post_url", column = "post_url"),
            @Result(property = "post_last_update", column = "last_update"),
            @Result(property = "vote_count", column = "vote_count"),
            @Result(property = "subreddit", column = "subreddit_id", many = @Many(select = "findSubredditById")),
            @Result(property = "comment", column = "post_id", many = @Many(select = "findCommentByPostId"))
    })
    public Post findPostById(int id);


    @Select("select * from post where subreddit_id = #{id}")
    @Results({
            @Result(property = "post_id", column = "post_id"),
            @Result(property = "post_title", column = "post_title"),
            @Result(property = "post_description", column = "post_description"),
            @Result(property = "post_url", column = "post_url"),
            @Result(property = "post_last_update", column = "last_update"),
            @Result(property = "vote_count", column = "vote_count"),
            @Result(property = "subreddit", column = "subreddit_id", many = @Many(select = "findSubredditById"))
    })
    public List<Post> findPostBySubredditId(int id);


    @Select("select * from comment where post_id = #{id}")
    public List<Comment> findCommentByPostId(int id);



    @Insert("insert into post (post_title,post_description, post_url,subreddit_id)" +
            "values(#{post.post_title},#{post.post_description}, #{post.post_url},#{post.subreddit_id})")
    public Boolean insertPost(@Param("post") Post post);

    @Update("update post set post_title=#{post.post_title}," +
            "post_description=#{post.post_description}, post_url=#{post.post_url},subreddit_id=#{post.subreddit_id}," +
            "vote_count=#{post.vote_count} where post_id=#{id}")
    public Boolean updatePost(@Param("post")Post post, int id);


    @Delete("delete from post where post_id=#{id}")
    public Boolean deletePost(int id);

    @Update("update post set vote_count=#{post.vote_count} where post_id=#{id}")
    public Boolean updateVote(@Param("post") Post post, int id);



}
