package com.example.g3_sr_redditclone.service;


import com.example.g3_sr_redditclone.model.Comment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {

    List<Comment> getAllComment();

    Comment findCommentById(int id);

    List<Comment> findCommentByPostId(int id);

    Boolean insertComment(Comment comment);

    void countComment();

    Boolean updateVote(@Param("comment") Comment comment, int id);
}
