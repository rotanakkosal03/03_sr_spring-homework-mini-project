package com.example.g3_sr_redditclone.service;


import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Post;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {


    List<Post> getAllPost();

    Boolean inserPost(Post post);

    Boolean updatePost(Post post,int id);

    Post findPostById(int id);

    Boolean deletePost(int id);

    List<Comment> findCommentBySubredditId(int id);

    List<Post> findPostBySubredditId(int id);

    List<Post> findCommentByPostId(int id);

    Boolean updateVote(Post post, int id);

}
