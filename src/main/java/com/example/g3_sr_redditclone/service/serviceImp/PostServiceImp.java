package com.example.g3_sr_redditclone.service.serviceImp;


import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Post;
import com.example.g3_sr_redditclone.repository.PostRepository;
import com.example.g3_sr_redditclone.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {

    @Autowired
    PostRepository postRepository;

    @Override
    public List<Post> getAllPost() {
        return postRepository.getAllPost();
    }

//    @Override
//    public List<Post> getPostAndSub() {
//        return postRepository.getPostAndComment();
//    }


    @Override
    public Boolean inserPost(Post post) {
        return postRepository.insertPost(post);
    }

    @Override
    public Boolean updatePost(Post post,int id) {
        return postRepository.updatePost(post,id);
    }

    @Override
    public Post findPostById(int id) {
        return postRepository.findPostById(id);
    }

    @Override
    public Boolean deletePost(int id) {
        return postRepository.deletePost(id);
    }

    @Override
    public List<Comment> findCommentBySubredditId(int id) {
        return postRepository.findCommentBySubredditId(id);
    }

    @Override
    public List<Post> findPostBySubredditId(int id) {
        return postRepository.findPostBySubredditId(id);
    }

    @Override
    public List<Post> findCommentByPostId(int id) {
        return null;
    }

    @Override
    public Boolean updateVote(Post post, int id) {
        return postRepository.updateVote(post,id);
    }


}
