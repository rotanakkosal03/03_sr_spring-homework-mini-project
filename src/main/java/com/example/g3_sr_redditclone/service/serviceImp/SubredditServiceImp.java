package com.example.g3_sr_redditclone.service.serviceImp;

import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Subreddit;
import com.example.g3_sr_redditclone.repository.SubredditRepository;
import com.example.g3_sr_redditclone.service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SubredditServiceImp implements SubredditService {

    @Autowired
    SubredditRepository subredditRepository;

    @Override
    public List<Subreddit> getAllSubreddit() {
        return subredditRepository.getAllSubreddit();
    }

    @Override
    public Subreddit findSubredditById(int id) {
        return subredditRepository.findSubredditById(id);
    }

    @Override
    public Boolean updateSubreddit(Subreddit subreddit, int id) {
        return subredditRepository.updateSubreddit(subreddit, id);
    }

    @Override
    public Boolean deleteSubreddit(int id) {
        return subredditRepository.deleteSubreddit(id);
    }

    @Override
    public Boolean insertSubreddit(Subreddit subreddit) {
        return subredditRepository.insertSubreddit(subreddit);
    }

    @Override
    public String getSubTitle(int id) {
        return subredditRepository.getSubTitle(id);
    }


}
